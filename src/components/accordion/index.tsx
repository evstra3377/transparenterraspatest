import { FC, useState, MouseEvent, useRef, useLayoutEffect } from "react";
import { IAccordion } from "./accordion.type";
import styles from "./accordion.module.scss";
import { getElementDimensions } from "../../utils/common";

export const Accordion: FC<IAccordion> = (props) => {
  const {
    defaultExpanded,
    expanded,
    children,
    onClick,
    className,
    label,
    ...rest
  } = props;
  const [contentHeight, setContentHeight] = useState<number>(0);
  const contentTextRef = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    if (contentTextRef.current) {
      setContentHeight(getElementDimensions(contentTextRef.current).height);
    }
  }, []);

  const handleClick = (event: MouseEvent<HTMLDivElement>) => {
    onClick?.(event, !expanded);
  };

  return (
    <div
      className={`${styles.accordion} ${className}`}
      onClick={handleClick}
      {...rest}
    >
      <div className={styles.header}>
        <label children={label} />
        <button>{expanded ? "–" : "+"}</button>
      </div>
      <div
        className={`${styles.content} ${expanded ? styles.expanded : ""}`}
        style={{ maxHeight: expanded ? contentHeight : undefined }}
      >
        <p ref={contentTextRef} className={styles.text} children={children} />
      </div>
    </div>
  );
};
