import { HTMLAttributes, MouseEvent } from "react";

export interface IAccordion
  extends Omit<HTMLAttributes<HTMLDivElement>, "onClick"> {
  defaultExpanded?: boolean;
  expanded: boolean;
  onClick?: (event: MouseEvent<HTMLDivElement>, expanded: boolean) => void;
  label?: string;
}
