export const getElementDimensions = (element: HTMLElement) => {
  const height = element.offsetHeight;
  const width = element.offsetWidth;
  const style = getComputedStyle(element);

  return {
    height: height + parseInt(style.marginTop) + parseInt(style.marginBottom),
    width: width + parseInt(style.marginLeft) + parseInt(style.marginRight),
  };
};
