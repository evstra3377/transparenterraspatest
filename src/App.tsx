import { Content } from "./templates/content";
import { Header } from "./templates/header";
import { Layout } from "./templates/layout";

function App() {
  return (
    <Layout>
      <Header />
      <Content />
    </Layout>
  );
}

export default App;
