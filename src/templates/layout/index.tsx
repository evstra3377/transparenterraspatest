//@ts-nocheck
import { FC, HTMLAttributes } from "react";
import videoUrl from "../../assets/video/Forest_89420.mp4";
import videoPreview from "../../assets/image/main-bg.jpg";
import styles from "./layout.module.scss";

export const Layout: FC<HTMLAttributes<HTMLDivElement>> = (props) => {
  return (
    <div className={styles.layout}>
      <div className={styles.overlay}></div>
      <video
        preload="auto"
        muted={true}
        autoPlay={true}
        loop={true}
        poster={videoPreview}
      >
        <source src={videoUrl} type="video/mp4" />
      </video>
      <div {...props} />
    </div>
  );
};
