export const data = [
  {
    label: `I. LEGAL AGREEMENT`,
    text: `These Terms of Service (the "Terms") constitute a legally binding
    agreement by and between FCE GROUP AG, the owner of website
    www.transparenterra.com (the "Website") (hereinafter, "we" or
    "Transparenterra" ) and you ("you" or "your") concerning your use of the
    Website and the services available through the Website (the "Services").
    By using the Website and Services, you represent and warrant that you
    have read and understood, and agree to be bound by, these Terms and
    Transparenterra Privacy Policy (the "Privacy Policy"), which is
    incorporated herein by reference. IF YOU DO NOT UNDERSTAND THESE TERMS,
    OR DO NOT AGREE TO BE BOUND BY IT OR THE PRIVACY POLICY, YOU MUST
    IMMEDIATELY LEAVE THE WEBSITE AND CEASE USING THE SERVICES.`,
  },
  {
    label: `II. PRIVACY POLICY`,
    text: `By using the Website, you consent to the collection and use of certain
    information about you, as specified in the Privacy Policy. We encourage
    users of the Website to frequently check Privacy Policy for changes.`,
  },
  {
    label: `III. CHANGES TO THESE TERMS AND PRIVACY POLICY`,
    text: `Internet technology and the applicable laws, rules, 
    and regulations change frequently. Accordingly, we reserve the right to change these 
    Terms and the Privacy Policy at any time upon notice to you to be given by posting of 
    a new version or a change notice on the Website. It is your responsibility to review 
    these Terms and the Privacy Policy periodically. If at any time you find either unacceptable, 
    you must immediately leave the Website and cease using the Services. 
    Unless Transparenterra obtains your express consent, any revised Privacy Policy will apply 
    only to information collected by Transparenterra after such time as the revised Privacy Policy takes effect, 
    and not to information collected under any earlier Privacy Policies..`,
  },
  {
    label: `IV. ADDITIONAL TERMS FOR SPECIFIC SERVICES`,
    text: `Additional terms and conditions listed on the Website or otherwise made available to you may apply to specific Services. 
    If you use those Services, then those additional terms become part of these Terms. 
    If any of the applicable additional terms conflict with these Terms, the additional terms will prevail while you are using the Services 
    to which they apply.`,
  },
  {
    label: `V. USER ELIGIBILITY`,
    text: `No one under 18 is allowed to create an account on the Website or use the Services. 
    By creating an account or using the Services you represent, warrant, 
    and agree that you can form a binding contract with Transparenterra.

    If you are using the Services on behalf of a business or some other entity, 
    you represent that you are authorized to bind that business or entity to these Terms and you agree 
    to these Terms on behalf of that business or entity (and all references to “you” and “your” in these 
        Terms will mean both you as the end-user and that business or entity).   
    `,
  },
  {
    label: `VI. USERS: MEMBERS AND VISITORS`,
    text: `When you register on the Website and join the Services, you become a member of 
    the Transparenterra community ("Transparenterra member"). As a Transparenterra member you can assume one or more roles described in 
    the Transparenterra Honor Code & Community Guideline. If you have chosen not to register for our Services, 
    you may access certain features of the Website as a “visitor”`,
  },
  {
    label: `VII. REGISTRATION, VERIFICATION, AND MEMBER PROFILE`,
    text: `To register on the Website you need to receive an invitation from another Transparenterra member and complete the registration 
    form by entering your personal information (alternatively, you can sign on through your profile on social networks). 
    You can choose to register as (1) an individual; or (2) a business owner; or (3) an expert; or (4) a professional investor; 
    with different access rights to various Services. In order to register as “professional investor” 
    you need to have a “qualified investor” or “professional investor” status under the applicable law in the jurisdiction of your domicile.
     We reserve the right to request from you the relevant documentary confirmation. 
     By registering as “professional investor” on the Website you represent and warrant that you have this legal status 
     under the law of your domicile

    Certain Services are available to members only upon completion of (a) the verification processed by Transparenterra;
     and/or (b) the KYC checks processed by third-party providers. 
     These processes are necessary to ensure trust and safety in the Transparenterra community. 
     As part of the verification/KYC process, you may be asked to provide copies of your passport, driver's license, 
     registration, tax, and other certificates for your business, and other documents. 
     We have the right to deny you verification without giving a reason.
    
    Upon registration, a member will create a public account that contains certain personal information. 
    The information you provide about yourself must be current, complete, and accurate and must not violate the law and third-party rights. 
    It is your responsibility to update your Transparenterra account as needed. Transparenterra does not, and cannot, 
    investigate information contained in member public profiles. Accordingly, Transparenterra does not represent, warrant, 
    or guarantee the currency or accuracy of public profile information, and hereby disclaims all responsibility and liability 
    for any information provided by Transparenterra members by means of public profiles or otherwise in connection with the Services
    
    By registering on the Website and each time you log on to Transparenterra, you warrant that you have all the powers 
    required to honor and execute these Terms.
    
    It is your sole responsibility to keep your login details confidential. 
    Everything performed through your Transparenterra account is considered as performed by you unless you report misuse of your account. 
    You agree to notify us immediately of any unauthorized use of your account, user name, or password. 
    Transparenterra shall not be liable for any loss that you incur as a result of someone else using your password, 
    either with or without your knowledge.
    
    By registering with the Website, you hereby consent to receive (1) periodic email communications regarding the Services, 
    new product offers, promotions, and other matters; and (2) electronic communications, 
    including email and instant messages from other Transparenterra members.`,
  },
  {
    label: `VIII. INTELLECTUAL PROPERTY RIGHTS`,
    text: `All copyright and other intellectual property rights subsisting in the Website and the Services, including without limitation all text, 
    images, graphics, and code contained in the Website, and in its look and feel (collectively, the "Contents") are owned by Transparenterra, 
    or by third-party providers. Transparenterra hereby grants you a worldwide, royalty-free, non-assignable, non-exclusive, revocable, 
    and non-sublicensable license to use the Website and the Services. 
    This license is for the sole purpose of using and enjoying the Website and the Services in a way that these Terms, 
    Privacy Policy and Transparenterra Honor Code & Community Guideline allow. Other than as specified above, 
    neither the Website nor any of the Contents may be modified or copied in whole or part in any form, including by framing, 
    incorporation into other websites or other publication, or be used to create any derivative work. 
    No links to the Website may be included in any other website without our prior written permission.`,
  },
  {
    label: `IX. THE WEBSITE DOES NOT PROVIDE PROFESSIONAL INVESTMENT ADVISORY SERVICES. RISK ASSUMPTION`,
    text: `Transparenterra provides the Website as a platform for experts, businesses, investors, and individuals where they can share thoughts, 
    information, and opinions with other users, purchase products and services, including professional expert advice, 
    participate in webinars and online conferences in accordance with these Terms and Transparenterra Honor Code & Community Guideline.

    The Website itself does not contain or constitute, and should not be interpreted as, financial, accounting, investment, 
    or other professional advice on any subject matter and is not a substitute for professional advice or opinion. 
    Transparenterra is not a fiduciary by virtue of any persons use of or access to the Website or Services. 
    You alone assume the sole responsibility of evaluating the merits and risks associated with the use of any information 
    on the Website before making any decisions based on such information. In exchange for using the Website, 
    you agree not to hold Transparenterra, its affiliates or any third party service provider liable for any possible claim for damages 
    rising from any decision you make based on information made available to you through the Website.
    
    Transparenterras role is to help connect Transparenterra members offering their services, such as professional expert advice, 
    with Transparenterra members seeking services. Transparenterra does not perform nor employs individuals to perform these services. 
    You acknowledge that Transparenterra does not supervise, direct, control, or monitor Transparenterra members 
    in the performance of these services and agree that:
    
    we are not responsible for the offering, performance, or procurement of these services,
    we do not endorse any members offered services, and
    nothing shall create an employment, agency, or joint venture relationship between us and any member offering services.
    This is the sole responsibility of the member seeking professional services and products via Transparenterra 
    to ensure that the member providing professional services is duly licensed, 
    has required legal status (such as status of professional investor under the relevant applicable laws) or otherwise authorized 
    to provide such services and products.
    
    If you are a Transparenterra member offering services or products, 
    by making such services and products available to other members via Transparenterra you represent 
    and warrant that you have all the required licenses, legal status, authorisations and expertise and will provide services consistent 
    with all applicable laws, these Terms and Transparenterra Honor Code & Community Guideline.
    
    Similarly, Transparenterra may help you register for and/or attend events organized by other Transparanterra members. 
    In this connection, you agree that:
    
    we are not responsible for the conduct of any of the members organizing such events,
    we do not endorse any event organized by any member, and
    we do not review and/or monitor any of these events.`,
  },
  {
    label: `X. RELIANCE ON THIRD-PARTY CONTENT`,
    text: `Opinions, advice, statements, or other information made available by means of the Website and Services by other Transparenterra 
    members and/or visitors, are those of their respective authors, and should not necessarily be relied on. 
    Such authors are solely responsible for such content. You acknowledge and understand that Transparenterra has not, 
    and does not, in any way: (a) screen its members; (b) inquire into the backgrounds of its members; 
    or (c) review or verify the statements of its members. By using the Services, 
    you may encounter content or information that might be inaccurate, incomplete, delayed, misleading, illegal, offensive or otherwise harmful. 
    You hereby agree to exercise reasonable precaution in all interactions with other members.`,
  },
  {
    label: `XI. THIRD-PARTY WEBSITES`,
    text: `The Website is linked with the websites of third parties ("Third-Party Websites"). 
    We do not have control over the content and performance of Third-Party Websites. 
    You agree that we have not reviewed, and cannot review or control, all of the material, 
    including computer software, goods, or services, made available on or through Third-Party Websites.`,
  },
  {
    label: `XII. USER CONTENT`,
    text: `"User Content" is any content, materials or information, that you upload or post to, or transmit, display, 
    perform or distribute by means o, the Website, whether in connection with your use of the Services or otherwise. 
    You hereby grant Transparenterra and its officers, directors, employees, agents, affiliates, representatives, sublicensees, successors, 
    and assigns (collectively, the "Transparenterra Parties") a perpetual, fully paid-up, worldwide, sublicensable, irrevocable,
    assignable license to copy, distribute, transmit, publicly display or perform, edit, translate, 
    reformat and otherwise use User Content in connection with the operation of the Website, the Services or any other similar or related business,
     in any medium now existing or later devised, including without limitation in advertising and publicity. 
     You further agree that the Transparenterra Parties may publish or otherwise disclose your name and/or any user name of yours 
     in connection with their exercise of the license granted under this section. You agree to waive, and hereby waive, 
     any claims arising from or relating to the exercise by the Transparenterra Parties of the rights granted under this section, 
     including without limitation any claims relating to your rights of personal privacy and publicity. 
     You will not be compensated for any exercise of the license granted under this section.

    By posting User Content on the Website you represent and warrant that in connection with your posted User Content:
    
    you are the creator and owner of it and have all necessary rights from other people or companies to use, and permit others to use it; and
    it does not infringe any third party right, including copyright and other intellectual property, privacy and rights of publicity;
    and you agree to hold the Transparenterra Parties harmless from any claims and indemnity them from any losses incurred by or asserted against 
    them and resulting from your breach of such representations and warranties..`,
  },
  {
    label: `XIII. PUBLIC FORUMS`,
    text: `"Public Forum" is any area, site, or feature offered as part of the Website (including without limitation public profiles, 
        discussion forums, message boards, blogs, chat rooms, emails, or instant messaging features) that enables you (a) to upload, submit,
        post, display, perform, distribute and/or view User Content, and/or (b) to communicate, share, 
        or exchange User Content with other Transparenterra members or other Website visitors. 
        You acknowledge that Public Forums and features contained therein, are for public and not private communications.
         You further acknowledge that anything you upload, submit, post, transmit, communicate, 
         share or exchange by means of any Public Forum may be viewed on the Internet by the general public, and therefore, 
         you have no expectation of privacy with regard to any such submission or posting (where we have made settings available, 
            we will honor the choices you make about who can see User Content or information). 
            You are, and shall remain, solely responsible for the User Content you upload, submit, post, transmit, communicate, 
            share or exchange by means of any Public Forum and for the consequences of submitting or posting the same. 
            Transparenterra disclaims any perceived, implied or actual duty to monitor Public Forums and specifically disclaims any responsibility 
            or liability for information provided thereon.`,
  },
  {
    label: `XIV. YOUR RESPONSIBILITY FOR DEFAMATORY COMMENTS`,
    text: `You agree and understand that you may be held legally responsible for damages suffered by other Transparenterra members, visitors, 
    or third parties as to the result of your comments, remarks, information, feedback or other User Content posted or made available 
    on the Website that is deemed defamatory or otherwise legally actionable. Transparenterra is not legally responsible, 
    nor can it be held liable for damages of any kind, arising out of or in connection to any defamatory or 
    otherwise legally actionable User Content posted or made available on the Website.`,
  },
  {
    label: `XV. PROHIBITED ACTIONS`,
    text: `When using the Website and the Services you hereby agree NOT TO:

    post any materials that violate any applicable law, including but not limited to libel, slander, antitrust, trademark, copyright, patent, 
    or unfair competition;
    advocate or discuss illegal activity;
    post anything which could encourage or facilitate discussions or any agreement that either expressly or impliedly leads to price fixing, 
    a boycott of another's business, or other conduct intended to illegally restrict free trade;
    post any materials that do not pertain to the discussion, topic or theme of the forum;
    infringe upon third-party copyright, nor invade the privacy or publicity rights of others;
    post anything protected by copyright without the permission of the copyright owner;
    post anything that is knowingly false, defamatory, profane, vulgar, obscene, threatening, abusive, hateful, bigoted, racially offensive, 
    pornographic, embarrassing, or that incite, encourage or threaten physical harm against another; promote or glorify racial intolerance, 
    use hate and/or racist terms, or signify hate towards any person or group of people; glamorize the use of hardcore illegal substances 
    and drugs; or has any potential to cause any harm or damage to anyone as determined by Transparenterra in its sole discretion;
    post anything of a sexual nature;
    use the Website for junk mail, spam, chain letters, or pyramid schemes;
    impersonate anyone or misrepresent who you are.
    Transparenterra has no obligation to, and does not in its normal course, prescreen, monitor or review User Content, 
    nor does it exert any editorial control over User Content. Without limiting any of its other remedies, 
    Transparenterra reserves the right to terminate your use of the Website and Services or your uploading, posting, transmission, display, 
    performance or distribution of User Content that is not in compliance with this section. Transparenterra, in its sole discretion, 
    may delete any such User Content from its servers. Transparenterra intends to cooperate fully with any law enforcement officials or 
    agencies in the investigation of any violation of these Terms or of any applicable laws`,
  },
  {
    label: `XVI. DISCLAIMER`,
    text: `Access to the Website, and use of the Services, is at your sole discretion and risk. 
    Whilst we have taken reasonable measures to ensure that the Website and its content is accurate and up-to-date, 
    we accept no responsibility for any action taken by any person or organization as a result, direct or otherwise, 
    of information contained in, or accessed through, the Website, whether provided by us or a third party. 
    The Website and the Services are provided on an "AS-IS" basis, and we make no warranties or representations about the Website or 
    any of the Services, including without limitation (i) the timeliness, currency, accuracy, completeness, reliability, continued availability, 
    or fitness for any particular purpose, (ii) that the use of the Website will be error-free, secure, and uninterrupted, 
    (iii) that we will correct any defects or that the Website will be free from viruses or other harmful codes, 
    and (iv) that the Services or the Website do not infringe any third-party rights. To the greatest extent permissible by applicable laws, 
    we exclude any such warranties and representations that may otherwise be implied and exclude all liability with respect to the Website, 
    the Services, or any use thereof..`,
  },
  {
    label: `XVII. LIMITATION OF LIABILITY`,
    text: `Transparenterra will not be liable to you for any direct, incidental, consequential, or similar damages arising (i) out of your access, 
    use, or inability to use the Website, the Services or any linked website, or (ii) in connection with any failure of performance, error, 
    omission, interruption, defect, delay in operation or transmission, computer virus or system failure.`,
  },
  {
    label: `XVIII. INDEMNIFICATION`,
    text: `You agree to indemnify and hold the Transparenterra Parties harmless from all liabilities, claims, 
    and expenses (including reasonable attorneys' fees and expenses) resulting from your (i) breach of these Terms, 
    (ii) misuse of the Website or any of the Services, or (iii) infringement of any of our intellectual-property rights..`,
  },
  {
    label: `XIX. GOVERNING LAW AND DISPUTE RESOLUTION`,
    text: `These Terms shall be governed by and construed in accordance with the law of Switzerland. Any dispute, controversy, 
    or claim arising out of, or in relation to, these Terms, including the validity, invalidity, breach, or termination thereof, 
    shall be resolved by arbitration in accordance with the Swiss Rules of International Arbitration of the Swiss Arbitration Centre 
    in force on the date on which the Notice of Arbitration is submitted in accordance with those Rules. The arbitration shall be held in Zurich, 
    Switzerland, and the arbitral proceeding shall be conducted in the English language..`,
  },
  {
    label: `XX. MODIFYING THE SERVICES AND TERMINATION`,
    text: `We are relentlessly improving our Services and creating new ones all the time. 
    That means we may add or remove features, products, or functionalities, and we may also suspend or stop the Services altogether.
     We may take any of these actions at any time for any reason, and when we do so, we may not provide you with any notice beforehand. 
     While we hope you remain a lifelong Transparenterra member, you can terminate these 
     Terms at any time and for any reason by deleting your Transparenterra account. 
     We may terminate or temporarily suspend your access to the Services if you fail to comply with these Terms, 
     Transparenterra Honor Code & Community Guideline, or the law without advanced notice. 
     That means that we may terminate these Terms, stop providing you with all or any part of the Services,
     or impose new or additional limits on your ability to use our Services. 
     And while well try to give you reasonable notice beforehand, we cant guarantee that notice will be possible in all circumstances.`,
  },
  {
    label: `XXI. NOTICES`,
    text: `All notices required or permitted to be given under these Terms must be in writing. 
    Transparenterra shall give any notice by email sent to the most recent email address listed in your Transparenterra profile. 
    You agree that any notice received from Transparenterra electronically satisfies any legal requirement that such notice be in writing.
     You bear the sole responsibility of ensuring that your email address on your Transparenterra profile is accurate and 
     current and notice to you shall be deemed effective upon the sending by Transparenterra of an email to that address. 
     You shall give any notice to Transparenterra by means of info@trasparenterra.com.`,
  },
  {
    label: `XXII. GENERAL`,
    text: `These Terms constitute the entire agreement between you and Transparenterra concerning your use of the Website and the Services. 
    These Terms may be modified unilaterally by Transparenterra by posting such amended version on the Website. 
    If any part of these Terms is held invalid or unenforceable, that part will be construed to reflect the parties original intent, 
    and the remaining portions will remain in full force and effect.`,
  },
];
