import { FC, HTMLAttributes, useState } from "react";
import { Accordion } from "../../components/accordion";
import styles from "./content.module.scss";
import { data } from "./data";

export const Content: FC<HTMLAttributes<HTMLDivElement>> = (props) => {
  const [expandedIndex, setExpandedIndex] = useState<number | undefined>(
    undefined
  );

  const handleAccordionClick = (expanded: boolean, index: number) => {
    setExpandedIndex(!expanded ? undefined : index);
  };

  return (
    <div {...props} className={`${styles.content} ${props.className}`}>
      <h1 className={styles.title}>TERMS OF SERVICES</h1>
      {data.map(({ label, text }, index) => (
        <Accordion
          label={label}
          expanded={index === expandedIndex}
          onClick={(e, expanded) => handleAccordionClick(expanded, index)}
        >
          {text}
        </Accordion>
      ))}
    </div>
  );
};
