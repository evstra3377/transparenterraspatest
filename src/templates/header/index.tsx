import { forwardRef, HTMLAttributes, useLayoutEffect, useState } from "react";
import styles from "./header.module.scss";
import signInUrl from "../../assets/image/exit-icon.png";
import menuUrl from "../../assets/image/menu-icon.png";
import logoUrl from "../../assets/image/logo-transparenterra-big.svg";

export const Header = forwardRef<
  HTMLDivElement,
  HTMLAttributes<HTMLDivElement>
>((props, ref) => {
  const [offset, setOffset] = useState(0);

  useLayoutEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });

    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <div
      {...props}
      ref={ref}
      className={`${styles.header} ${offset ? styles.fixed : ""} ${
        props.className
      }`}
    >
      <div className={styles.logo}>
        <a href="/">
          <img src={logoUrl} alt="X" />
        </a>
      </div>
      <div className={styles.actions}>
        <a href="/sign in">
          <img src={signInUrl} alt="X" />
          <span>Sign in</span>
        </a>
        <a href="/menu">
          <img src={menuUrl} alt="X" />
          <span>MENU</span>
        </a>
      </div>
    </div>
  );
});
